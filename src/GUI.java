import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class GUI 
{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	// Displays enemy space ships when undo button is pressed
	private void displayEnemySpaceShipsUndo(ArrayList<JPanel> panelList, GameController gameController, ImageIcon battleStarImg, ImageIcon battleCruiserImg, ImageIcon battleShooterImg)
	{
		for (int i = 0; i < panelList.size(); i++) 
		{

			for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
			{

				// ADD IMAGES
				if (gameController.enemies.get(currentEnemy).getPosition() == i)
				{	
					JLabel tempLabel = new JLabel();

					if (gameController.enemies.get(currentEnemy) instanceof BattleStar)
					{
						tempLabel.setIcon(battleStarImg);
					}
					else if (gameController.enemies.get(currentEnemy) instanceof BattleShooter)
					{
						tempLabel.setIcon(battleShooterImg);
					}
					else if (gameController.enemies.get(currentEnemy) instanceof BattleCruiser)
					{
						tempLabel.setIcon(battleCruiserImg);
					}
					if (gameController.enemies.get(currentEnemy).getPosition() == gameController.enemies.get(currentEnemy).prevPos)
					{
						continue;
					}
					else
					{
						panelList.get(i).add(tempLabel);
						panelList.get(i).setVisible(true);

						panelList.get(i).revalidate();
						panelList.get(i).repaint();
					}
				}
			}
		}

		//TODO: DELETE POSITIONS WHERE ENEMY SPACE SHIPS ARE NOT IN
		for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
		{
			if (panelList.get(gameController.enemies.get(currentEnemy).prevPos).getComponentCount() > 0)
			{
				//if (gameController.enemies.get(currentEnemy).positionHistory.size() <=2)
				//	continue;

				if (gameController.enemies.get(currentEnemy).prevPos == 0 && gameController.enemies.get(currentEnemy).shouldDelete == false)
				{
					continue;
				}
				for (int j = 0; j < panelList.get(gameController.enemies.get(currentEnemy).prevPos).getComponentCount(); j++) 
				{
					JLabel asd = (JLabel) panelList.get(gameController.enemies.get(currentEnemy).prevPos).getComponent(j);
					if (asd.getIcon().equals(battleStarImg) || asd.getIcon().equals(battleCruiserImg) || asd.getIcon().equals(battleShooterImg))
					{
						Container parent = asd.getParent();
						parent.remove(asd);
						parent.validate();
						parent.repaint();
					}
				}

			}
		}

	}

	// Clears the entire grid
	private void clearBoard(ArrayList<JPanel> panelList)
	{
		for (int i = 0; i < panelList.size(); i++) 
		{
			if (panelList.get(i).getComponentCount() > 0)
			{
				panelList.get(i).removeAll();
			}
			frame.revalidate();
			frame.repaint();
		}
	}

	// Method that displays the enemy space ships
	private void displayEnemySpaceShips(ArrayList<JPanel> panelList, GameController gameController, ImageIcon battleStarImg, ImageIcon battleCruiserImg, ImageIcon battleShooterImg)
	{
		for (int i = 0; i < panelList.size(); i++) 
		{
			// ENEMY SPACE SHIP CONTROL
			for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
			{
				// ADD IMAGES
				if (gameController.enemies.get(currentEnemy).getPosition() == i)
				{
					JLabel tempLabel = new JLabel();

					if (gameController.enemies.get(currentEnemy) instanceof BattleStar)
					{
						tempLabel.setIcon(battleStarImg);
					}
					else if (gameController.enemies.get(currentEnemy) instanceof BattleShooter)
					{
						tempLabel.setIcon(battleShooterImg);
					}
					else if (gameController.enemies.get(currentEnemy) instanceof BattleCruiser)
					{
						tempLabel.setIcon(battleCruiserImg);
					}

					panelList.get(i).add(tempLabel);
					panelList.get(i).setVisible(true);

					panelList.get(i).revalidate();
					panelList.get(i).repaint();
				}
			}

			frame.revalidate();
			frame.repaint();
		}

		for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
		{
			if (panelList.get(gameController.enemies.get(currentEnemy).GetPreviousShipPositionFromHistory()).getComponentCount() > 0)
			{
				if (gameController.enemies.get(currentEnemy).history.size() <=1)
					continue;

				//if (gameController.enemies.get(currentEnemy).positionHistory.size() <=2)
				//	continue;


				JLabel asd = (JLabel) panelList.get(gameController.enemies.get(currentEnemy).GetPreviousShipPositionFromHistory()).getComponent(0);
				if (asd.getIcon().equals(battleStarImg) || asd.getIcon().equals(battleCruiserImg) || asd.getIcon().equals(battleShooterImg))
				{
					Container parent = asd.getParent();
					parent.remove(asd);
					parent.validate();
					parent.repaint();
				}
			}
		}
	}

	// Method that displays the master space ship on GUI
	private void displayMss(Ship mss, ArrayList<JPanel> panelList, ImageIcon mssImg)
	{
		for (int i = 0; i < panelList.size(); i++) 
		{
			// MASTER SPACE SHIP DISPLAY CONTROL
			// ADD IMAGES
			if (mss.getPosition() == i)
			{
				// DON'T ADD MASTER SPACE SHIP IMAGE TWICE
				if (panelList.get(i).getComponentCount() > 0)
				{
					Boolean skip = false;
					for (int j = 0; j < panelList.get(i).getComponentCount(); j++) 
					{
						JLabel asd = (JLabel) panelList.get(i).getComponent(j);
						if (asd.getIcon().equals(mssImg))
						{
							skip = true;
						}
					}
					if (skip)
					{
						continue;
					}
				}

				JLabel tempLabel = new JLabel();
				tempLabel.setIcon(mssImg);
				panelList.get(i).add(tempLabel);
				panelList.get(i).setVisible(true);
			}
			// REMOVE IMAGES
			else
			{
				if (panelList.get(i).getComponentCount() > 0)
				{
					for (int j = 0; j < panelList.get(i).getComponentCount(); j++) 
					{

						JLabel asd = (JLabel) panelList.get(i).getComponent(j);
						if (asd.getIcon().equals(mssImg))
						{
							Container parent = asd.getParent();
							parent.remove(j);
							parent.validate();
							parent.repaint();
						}
					}
				}
			}
			frame.revalidate();
			frame.repaint();
		}
	}

	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel(new ImageIcon(GUI.class.getResource("images/background2.png")));
		
		System.out.println(GUI.class.getResource("background2.png"));
		frame.setContentPane(label);
		frame.setTitle("Sky Wars");
		frame.getContentPane().setLayout(new GridLayout(5, 4, 0, 0));
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		GameController gameController = new GameController();

		ShipFactory SF = new ShipFactory();

		MasterSpaceShip mss = (MasterSpaceShip) SF.CreateMasterSpaceShip("masterspaceship");

		mss.RegisterObserver(gameController);

		ArrayList<JPanel> panelList = new ArrayList<JPanel>();


		//labelList.add(Cell1);
		for (int i = 0; i < 16; i++) 
		{
			JPanel tempCell = new JPanel();
			frame.getContentPane().add(tempCell);
			panelList.add(tempCell);
			panelList.get(i).setOpaque(false);
			panelList.get(i).revalidate();
			panelList.get(i).repaint();

		}
		// Game images
		ImageIcon mssImg = new ImageIcon(GUI.class.getResource("images/mss64.png"));

		ImageIcon battleStarImg = new ImageIcon(GUI.class.getResource("images/battleStar64.png"));
		ImageIcon battleCruiserImg = new ImageIcon(GUI.class.getResource("images/battleCruiser64.png"));
		ImageIcon battleShooterImg = new ImageIcon(GUI.class.getResource("images/battleShooter64.png"));
		ImageIcon explosion = new ImageIcon(GUI.class.getResource("images/explosion3.gif"));

		// Game sounds
		final String moveSound = "warp.wav";
		final String undoSound = "warp reverse.wav";
		final String destroySound = "explosion.wav";
		final String gameOverSound = "game over.wav";

		JPanel panel = new JPanel();
		label.add(panel);
		panel.setOpaque(false);

		// Start game button
		JButton StartGame = new JButton("Start Game");
		panel.add(StartGame);
		// Switch mode button
		JToggleButton tglbtnSwitchMode = new JToggleButton("Switch mode");
		panel.add(tglbtnSwitchMode);
		// Make move button
		tglbtnSwitchMode.setEnabled(false);
		JButton btnMakeMove = new JButton("Make Move");
		panel.add(btnMakeMove);
		btnMakeMove.setEnabled(false);
		// Undo move button
		JButton btnUndoMove = new JButton("Undo Move");
		panel.add(btnUndoMove);
		btnUndoMove.setEnabled(false);

		// Label for displaying master space ship operational mode
		JLabel lblMssMode = new JLabel("Mode: Defensive");
		lblMssMode.setForeground(Color.WHITE);
		panel.add(lblMssMode);

		// Undo move action listener
		btnUndoMove.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				AudioManager player = new AudioManager();

				//clearBoard(panelList);

				mss.undoMove();

				gameController.checkForEnemyRevival(mss.history);

				gameController.checkForEnemyDestruction();

				if (gameController.enemies.size() > 0)
				{
					for(Ship enemy: gameController.enemies)
					{
						enemy.undoMove();
					}

				}


				//clearBoard(panelList);
				displayMss(mss, panelList, mssImg);
				displayEnemySpaceShipsUndo(panelList, gameController, battleStarImg, battleCruiserImg, battleShooterImg);

				player.play(undoSound);

				if (panelList.get(0).getComponentCount() > 0 && gameController.enemies.size() < 1)
				{
					panelList.get(0).removeAll();
				}


			}
		}
				);
		// Make move action listener
		btnMakeMove.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				Thread randomShip = null;
				AudioManager player = new AudioManager();
				if (gameController.CheckForEnemyShipCreation())
				{
					randomShip = SF.CreateRandomEnemyShip(gameController);
					randomShip.run();
				}

				MakeMoveCommand move = new MakeMoveCommand(mss);
				mss.storeAndExecute(move, false);

				player.play(moveSound);

				if (gameController.enemies.size() > 0)
				{
					for(Ship enemy: gameController.enemies)
					{
						if (enemy.justSpawned == false)
						{
							MakeMoveCommand moveEnemy = new MakeMoveCommand(enemy);
							enemy.storeAndExecute(moveEnemy, false);
						}
					}
				}
				// CONFLICT RESOLUTION

				int enemiesInSameCell = 0;
				for (Ship enemy : gameController.enemies) 
				{
					if (enemy.position == mss.position)
					{
						enemiesInSameCell++;
					}
				}


				// get rid of explosions from previous moves
				for (int i = 0; i < panelList.size(); i++) 
				{
					if (panelList.get(i).getComponentCount() > 0)
					{
						for (int j = 0; j < panelList.get(i).getComponentCount(); j++) //  j < panelList.get(gameController.enemies.get(currentEnemy).GetPreviousShipPosition()).getComponentCount();
						{
							JLabel asd = (JLabel) panelList.get(i).getComponent(j);
							if (asd.getIcon().equals(explosion))
							{
								Container parent = asd.getParent();
								parent.remove(asd);
								parent.validate();
								parent.repaint();
							}
						}
					}
				}


				displayMss(mss,panelList, mssImg);
				displayEnemySpaceShips(panelList, gameController, battleStarImg, battleCruiserImg, battleShooterImg);

				// Enemies can move next turn
				if (gameController.enemies.size() > 0)
				{
					for(Ship enemy: gameController.enemies)
					{
						enemy.justSpawned = false;
					}
				}

				if (enemiesInSameCell >= 2)
				{

					if (!(mss.getMssStatus(enemiesInSameCell)))
					{
						// end game
						player.play(gameOverSound);
						System.out.println("-------------------------------- G A M E  O V E R --------------------------------");
						JOptionPane.showMessageDialog(frame, "                          "
								+ "G A M E  O V E R \n Y O U  W E R E  D E S T R O Y E D  B Y  " 
								+ enemiesInSameCell + "  E N E M I E S");
						gameController.Reset();
						clearBoard(panelList);
						mss.Reset();
						StartGame.setEnabled(true);

						//if (mss.mssMode instanceof Offensive)
						//	tglbtnSwitchMode.doClick();

						tglbtnSwitchMode.setEnabled(false);

						btnMakeMove.setEnabled(false);
						btnUndoMove.setEnabled(false);
					}
					else
					{
						// DELETE ENEMY SPACE SHIP
						for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
						{
							if (gameController.enemies.get(currentEnemy).position == mss.position)
							{
								for (int j = 0; j < panelList.get(mss.position).getComponentCount(); j++) //  
								{
									JLabel asd = (JLabel) panelList.get(mss.position).getComponent(j);
									if (asd.getIcon().equals(battleStarImg) || asd.getIcon().equals(battleCruiserImg) || asd.getIcon().equals(battleShooterImg))
									{
										Container parent = asd.getParent();
										parent.remove(asd);
										parent.validate();
										parent.repaint();
									}
								}
								gameController.DestroyEnemy(gameController.enemies.get(currentEnemy), mss.history);
								//gameController.enemies.remove(currentEnemy);
								frame.revalidate();
								frame.repaint();

								JLabel tempLabel = new JLabel();
								tempLabel.setIcon(explosion);
								panelList.get(mss.position).add(tempLabel);
								panelList.get(mss.position).setVisible(true);

								panelList.get(mss.position).revalidate();
								panelList.get(mss.position).repaint();

								player.play(destroySound);
							}
						}
					}

				}
				else
				{
					// DELETE ENEMY SPACE SHIP
					for (int currentEnemy = 0; currentEnemy < gameController.enemies.size(); currentEnemy++) 
					{

						if (gameController.enemies.get(currentEnemy).position == mss.position)
						{
							for (int j = 0; j < panelList.get(mss.position).getComponentCount(); j++) //  
							{
								JLabel asd = (JLabel) panelList.get(mss.position).getComponent(j);
								if (asd.getIcon().equals(battleStarImg) || asd.getIcon().equals(battleCruiserImg) || asd.getIcon().equals(battleShooterImg))
								{
									Container parent = asd.getParent();
									parent.remove(asd);
									parent.validate();
									parent.repaint();
								}
							}

							gameController.DestroyEnemy(gameController.enemies.get(currentEnemy), mss.history);

							frame.revalidate();
							frame.repaint();


							JLabel tempLabel = new JLabel();
							tempLabel.setIcon(explosion);
							panelList.get(mss.position).add(tempLabel);
							panelList.get(mss.position).setVisible(true);

							panelList.get(mss.position).revalidate();
							panelList.get(mss.position).repaint();

							player.play(destroySound);

						}
					}
				}

			}


		}
				);
		// Switch mode action listener
		tglbtnSwitchMode.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// Switch mode
				mss.SwitchMode();
				if (mss.mssMode instanceof Offensive)
					lblMssMode.setText("Mode: Offensive");
				else
					lblMssMode.setText("Mode: Defensive");

			}
		});

		// Start game action listener
		StartGame.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{

				StartGame.setEnabled(false);
				// Game started enable buttons
				tglbtnSwitchMode.setEnabled(true);
				btnMakeMove.setEnabled(true);
				btnUndoMove.setEnabled(true);

				MakeMoveCommand move = new MakeMoveCommand(mss);
				mss.storeAndExecute(move, false);

				for (int i = 0; i < panelList.size(); i++) 
				{

					if (gameController.GetMasterSpaceShipPosition() == i)
					{
						JLabel tempLabel = new JLabel();
						tempLabel.setIcon(mssImg);
						panelList.get(i).add(tempLabel);
						frame.setVisible(true);
					}
				}

			}
		});
	}
}
