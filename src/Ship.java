import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public abstract class Ship implements Runnable
{
	protected int position;
	public int prevPos;
	public boolean justSpawned = true;
	public boolean shouldDelete = false;

	public ArrayList<MakeMoveCommand> history = new ArrayList<MakeMoveCommand>();
	public ArrayList<MakeMoveCommand> mssCommandsAtDeath = new ArrayList<MakeMoveCommand>();

	private GameController observer;

	public void storeAndExecute(MakeMoveCommand cmd, boolean first) 
	{
		this.history.add(cmd);
		cmd.execute(first);
	}
	
	public void undoMove()
	{
		
		if (history.size() > 1)
		{
			MakeMoveCommand previousCommand;
			this.prevPos = history.get(history.size() - 1).position;
			history.remove(history.size() - 1);
		
			previousCommand = history.get(history.size() - 1);
			this.position = previousCommand.position;
		}
		else if (history.size() == 1)
		{
			MakeMoveCommand previousCommand;
			previousCommand = history.get(0);
			this.prevPos = history.get(0).position;
			this.position = previousCommand.position;
			this.shouldDelete = true;
		}
		
	}

	public int getPosition() 
	{
		
		return history.get(history.size() - 1).position;
		
	}

	public void CalculateNextPosition(Boolean first)
	{
		int displacement = ThreadLocalRandom.current().nextInt(-5, 6);

		int newPosition;
		if (first)
		{
			newPosition = 0;
		}
		else
		{
			while (Math.abs(displacement) == 2 || displacement == 0 ||
					this.position + displacement <= 0 || this.position + displacement >= 16 ||
					(this.position == 0 || this.position == 4 || this.position == 8 || this.position == 12) && (displacement == -5 || displacement == 3 || displacement == -1 ) ||
					(this.position == 3 || this.position == 7 || this.position == 11 || this.position == 15) && (displacement == 1 || displacement == -3) || displacement == 5)
			{
				// Get a random num
				displacement = ThreadLocalRandom.current().nextInt(-5, 6);
			}
			newPosition = displacement + position;
		}

		Move(newPosition);

	}


	
	public int GetPreviousShipPositionFromHistory()
	{
		if (this.history.size() == 1)
			return this.history.get(0).position;
		return this.history.get(this.history.size() - 2).position;
	}
	


	public void Move(int position)
	{
		if (history.size() > 0)
			history.get(history.size()-1).position = position;
		this.position = position;

		NotifyObserver(); // Notify observer of position change
	}

	public void RegisterObserver(GameController gameController)
	{
		observer = gameController;
	}

	public void RemoveObserver()
	{
		observer = null;
	}

	public void NotifyObserver()
	{
		observer.Update(this);
	}

}
