import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class GameController 
{

	private ArrayList<Integer> masterSpaceShipPositionHistory; // Used to keep track of master space ship position
	public ArrayList<Ship> enemies;
	public ArrayList<Ship> destroyedEnemies;

	public GameController()
	{
		enemies = new ArrayList<Ship>();
		masterSpaceShipPositionHistory = new ArrayList<Integer>();
		destroyedEnemies = new ArrayList<Ship>();
	}

	// Update is called when MasterSpaceShip Moves
	public void Update(Ship ship)
	{
		// If current ship is a master space ship
		if(ship instanceof MasterSpaceShip)
		{
			this.masterSpaceShipPositionHistory.add(ship.position); 
		}

		// If current ship is a battlecruiser
		if(ship instanceof BattleCruiser || ship instanceof BattleShooter || ship instanceof BattleStar)
		{
			if (!(enemies.contains(ship)))
			{
				//ship.position = 0;
				enemies.add(ship);
			}
			else
			{
				for (int i = 0; i < enemies.size(); i++) 
				{
					if (enemies.get(i).equals(ship))
					{
						enemies.get(i).position = ship.position;
					}
				}
			}
		}
	}
	
	public void DestroyEnemy(Ship enemy, ArrayList<MakeMoveCommand> mssCommandsAtDeath)
	{
		enemy.mssCommandsAtDeath = mssCommandsAtDeath;
		this.destroyedEnemies.add(enemy);
		this.enemies.remove(enemy);
	}
	
	public void checkForEnemyRevival(ArrayList<MakeMoveCommand> mssCommandsCurrent)
	{
		for (int i = 0; i < destroyedEnemies.size(); i++) 
		{
			if (mssCommandsCurrent.equals(destroyedEnemies.get(i).mssCommandsAtDeath))
			{
				enemies.add(destroyedEnemies.get(i));
				destroyedEnemies.remove(destroyedEnemies.get(i));
			}
		}
	}
	
	public void checkForEnemyDestruction()
	{
		for (int i = 0; i < this.enemies.size(); i++) 
		{
			if (this.enemies.get(i).shouldDelete && this.enemies.get(i).getPosition() == 0)
			{
				this.enemies.remove(i);
			}
		}
	}

	public int GetMasterSpaceShipPosition()
	{
		// Return last entry
		return this.masterSpaceShipPositionHistory.get(masterSpaceShipPositionHistory.size()-1);
	}

	public int GetPreviousMasterSpaceShipPosition()
	{
		int thePosition;
		if(masterSpaceShipPositionHistory.size() - 1 <= 1)
		{
			thePosition = this.masterSpaceShipPositionHistory.get(0);

			if (this.masterSpaceShipPositionHistory.size() > 1)
				masterSpaceShipPositionHistory.remove(masterSpaceShipPositionHistory.size() - 1);

			return thePosition;
		}
		thePosition = this.masterSpaceShipPositionHistory.get(masterSpaceShipPositionHistory.size() - 2);
		masterSpaceShipPositionHistory.remove(masterSpaceShipPositionHistory.size() - 1);
		return thePosition;
	}

	// 1 in 3 possibility that enemy ship should be created
	public boolean CheckForEnemyShipCreation()
	{
		int randomNum = ThreadLocalRandom.current().nextInt(1, 4);

		if (randomNum == 1)
			return true;
		return false;

	}

	public void Reset()
	{
		enemies.clear();
		masterSpaceShipPositionHistory.clear();
	}



}
