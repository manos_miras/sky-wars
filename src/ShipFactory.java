import java.util.concurrent.ThreadLocalRandom;

public class ShipFactory 
{
	public Thread CreateEnemyShip(String type, GameController gameController) 
	{
		Thread ship = null;
		
		if(type.equalsIgnoreCase("battlecruiser")) 
		{
			ship = new Thread(new BattleCruiser(gameController));
		}
		if(type.equalsIgnoreCase("battleshooter")) 
		{
			ship = new Thread(new BattleShooter(gameController));
		}
		if(type.equalsIgnoreCase("battlestar")) 
		{
			ship = new Thread(new BattleStar(gameController));
		}
		return ship;
	}
	
	public Ship CreateMasterSpaceShip(String type)
	{
		Ship ship = null;
		if(type.equalsIgnoreCase("masterspaceship")) 
		{
			ship = new MasterSpaceShip();
		}
		return ship;
	}
	
	public Thread CreateRandomEnemyShip(GameController gameController)
	{
		
		int randomNum = ThreadLocalRandom.current().nextInt(1, 4);
		String shipToBeMade = "";
		switch(randomNum)
		{
			case 1: shipToBeMade = "battlecruiser";
				break;
			case 2: shipToBeMade = "battleshooter";
				break;
			case 3: shipToBeMade = "battlestar";
				break;
			default: shipToBeMade = "battlecruiser";
            	break;
		}
		Thread ship = CreateEnemyShip(shipToBeMade, gameController);
		return ship;
	}

}
