public class Offensive implements IOperationalMode 
{

	@Override
	public boolean CalculateMasterSpaceShipStatus(int enemiesInSamePos) 
	{
		if (enemiesInSamePos >= 3)
		{
			// Master Space Ship is destroyed
			return false;
		}

		return true;
	}

}
