public class BattleStar extends Ship
{
	GameController gameController;
	public BattleStar()
	{

	}
	
	public BattleStar(GameController gameController)
	{

		this.gameController = gameController;
	}

	@Override
	public void run() 
	{
		this.RegisterObserver(gameController);

		MakeMoveCommand move = new MakeMoveCommand(this);
		this.storeAndExecute(move, true);
		this.CalculateNextPosition(true);
	}
}
