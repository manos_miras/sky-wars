
public interface ICommand 
{
	
	void execute(boolean first);
	
	void undo();
}
