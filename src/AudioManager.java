import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioManager implements LineListener 
{

    boolean playCompleted;
     
    //Play a given audio file.
    void play(String audioFilePath) 
    { 
        try 
        {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(GUI.class.getClassLoader().getResource(audioFilePath));
 
            AudioFormat format = audioStream.getFormat();
 
            DataLine.Info info = new DataLine.Info(Clip.class, format);
 
            Clip audioClip = (Clip) AudioSystem.getLine(info);
 
            audioClip.addLineListener(this);
 
            audioClip.open(audioStream);
             
            audioClip.start();

            //audioClip.close(); // Memory leak?
             
        } 
        catch (UnsupportedAudioFileException ex) 
        {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        }
        catch (LineUnavailableException ex) 
        {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } 
        catch (Exception ex) 
        {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }
         
    }
    
    @Override
    public void update(LineEvent event) 
    {
        /*
    	LineEvent.Type type = event.getType();
         
        if (type == LineEvent.Type.START) {
            System.out.println("Playback started.");
             
        } else if (type == LineEvent.Type.STOP) {
            playCompleted = true;
            System.out.println("Playback completed.");
        }
        */
    }
}