public class BattleShooter extends Ship
{
	GameController gameController;
	public BattleShooter()
	{

	}
	
	public BattleShooter(GameController gameController)
	{

		this.gameController = gameController;
	}
	
	@Override
	public void run() 
	{
		this.RegisterObserver(gameController);
		MakeMoveCommand move = new MakeMoveCommand(this);
		this.storeAndExecute(move, true);
		this.CalculateNextPosition(true);
	}
}
