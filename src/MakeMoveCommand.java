
public class MakeMoveCommand implements ICommand 
{
	private Ship theShip;
	int position;
	public MakeMoveCommand(Ship theShip)
	{
		this.theShip = theShip;
	}
	
	@Override
	public void execute(boolean first) 
	{
		theShip.CalculateNextPosition(first);
	}
	
	@Override
	public void undo()
	{
		theShip.undoMove();
	}
}
