
public class Defensive implements IOperationalMode 
{
	@Override
	public boolean CalculateMasterSpaceShipStatus(int enemiesInSamePos) 
	{
		if (enemiesInSamePos >= 2)
		{
			// Master Space Ship is destroyed
			return false;
		}

		return true;
	}

}
