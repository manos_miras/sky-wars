public class MasterSpaceShip extends Ship 
{

	public IOperationalMode mssMode;
	
	public MasterSpaceShip()
	{
		this.setMssMode(new Defensive()); // Set default operational mode as Defensive
	}
	
	public void SwitchMode()
	{
		if (mssMode instanceof Defensive)
		{
			this.setMssMode(new Offensive());
		}
		else
		{
			this.setMssMode(new Defensive());
		}
	}
	
	public boolean getMssStatus(int enemiesInSamePos)
	{
		return mssMode.CalculateMasterSpaceShipStatus(enemiesInSamePos);
	}
	
	public void setMssMode(IOperationalMode mssMode)
	{
		this.mssMode = mssMode;
	}
	
	public void Reset()
	{
		this.history.clear();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
}
